package com.tsc.smironova.tm.api.controller;

public interface ICommandController {

    void showIncorrectArgument(final String arg);

    void showIncorrectCommand(final String command);

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showInfo();

    void exit();

}
