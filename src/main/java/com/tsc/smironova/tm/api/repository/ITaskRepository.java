package com.tsc.smironova.tm.api.repository;

import com.tsc.smironova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> taskComparator);

    int size();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    List<Task> findAllByProjectId(String projectId);

    List<Task> removeAllByProjectId(String projectId);

    Task bindTaskToProject(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

}
