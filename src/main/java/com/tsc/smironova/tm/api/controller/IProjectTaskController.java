package com.tsc.smironova.tm.api.controller;

import com.tsc.smironova.tm.model.Project;

public interface IProjectTaskController {

    void findAllTasksByProjectId();

    void bindTaskToProject();

    void unbindTaskFromProject();

    void showProject(Project project);

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void clearProjects();

}
