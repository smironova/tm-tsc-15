package com.tsc.smironova.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
