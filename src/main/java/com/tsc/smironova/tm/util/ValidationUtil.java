package com.tsc.smironova.tm.util;

import com.tsc.smironova.tm.enumerated.Sort;
import com.tsc.smironova.tm.enumerated.Status;

import java.util.List;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean isEmpty(final List value) {
        return value == null || value.isEmpty();
    }

    static boolean isEmpty(final Status value) {
        return value == null;
    }

    static boolean isEmpty(final Integer index) {
        return index == null;
    }

    static boolean checkIndex(final Integer index) {
        return index < 0;
    }

    static boolean checkIndex(final Integer index, final int size) {
        return index > size - 1;
    }

    static boolean checkSort(final String value) {
        for (Sort sort : Sort.values()) {
            if (sort.name().equals(value))
                return false;
        }
        return true;
    }

}
