package com.tsc.smironova.tm.exception.system;

import com.tsc.smironova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! This value is incorrect...");
    }

    public IndexIncorrectException(final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IndexIncorrectException(final Integer value) {
        super("Error! This value ``" + value.toString() + "`` is not positive integer...");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

}
