package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyTaskList extends AbstractException {

    public EmptyTaskList() {
        super("No tasks! Add new task...");
    }

}
