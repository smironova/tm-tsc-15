package com.tsc.smironova.tm.exception.system;

import com.tsc.smironova.tm.exception.AbstractException;

public class UnknownSortException extends AbstractException {

    public UnknownSortException(final String sort) {
        super("Error! Sort ``" + sort + "`` was not found...");
    }

}
