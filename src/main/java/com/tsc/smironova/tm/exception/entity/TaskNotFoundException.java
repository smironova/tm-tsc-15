package com.tsc.smironova.tm.exception.entity;

import com.tsc.smironova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task was not found...");
    }

}
